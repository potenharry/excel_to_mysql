# excel_to_mysql
---

재무재표의 당기순이익, 자본총계와 같은 포맷의 엑셀파일의 데이터를 파싱하여 sql query를 만들어 txt 파일로 만든다.
그리고 이 파일의 query들을 mysql 특정 table에 업데이트한다. 