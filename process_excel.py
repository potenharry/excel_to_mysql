import pandas as pd
import math

# code 추출
# cols = [0]
# df = pd.read_excel('juju.xlsx', sheet_name='자본총계', header=11, usecols=cols)
# print(df)


def get_total_equity_query():
    update_query1 = "UPDATE osh_fun SET total_equity="
    update_query2 = " WHERE code="
    update_query3 = " and qt="

    df = pd.read_excel('juju.xlsx', sheet_name='자본총계', skiprows=9, index_col=0)
    period = list(df)[2:]

    df = pd.read_excel('juju.xlsx', sheet_name='자본총계', skiprows=12)
    codes = list(df['Code'])

    df = pd.read_excel('juju.xlsx', sheet_name='자본총계', skiprows=12, index_col='Code')

    with open('total_equity.txt', 'a') as f:
        for code in codes:
            val = list(df.loc[code].values)[2:]

            for idx, date in enumerate(period):
                # temp_dict = {str(date): val[idx]}
                # val_set.append(temp_dict)
                if math.isnan(val[idx]) is False:
                    query = update_query1 + str(val[idx]) + update_query2 + "'" + str(code) + "'" + update_query3 + str(date) + ';'
                    f.write(str(query) + '\n')


def get_net_profit_query():
    update_query1 = "UPDATE osh_fun SET net_profit="
    update_query2 = " WHERE code="
    update_query3 = " and qt="

    df = pd.read_excel('juju.xlsx', sheet_name='당기순이익', skiprows=9, index_col=0)
    period = list(df)[2:]

    df = pd.read_excel('juju.xlsx', sheet_name='당기순이익', skiprows=12)
    codes = list(df['Code'])

    df = pd.read_excel('juju.xlsx', sheet_name='당기순이익', skiprows=12, index_col='Code')

    with open('net_profit.txt', 'a') as f:
        for code in codes:
            val = list(df.loc[code].values)[2:]

            for idx, date in enumerate(period):
                # temp_dict = {str(date): val[idx]}
                # val_set.append(temp_dict)
                if math.isnan(val[idx]) is False:
                    query = update_query1 + str(val[idx]) + update_query2 + "'" + str(code) + "'" + update_query3 + str(
                        date) + ';'
                    f.write(str(query) + '\n')


if __name__ == '__main__':
    get_total_equity_query()
    get_net_profit_query()


# row_period = pd.DataFrame(period)
# row_period.reset_index(drop=True)
# header_code = pd.DataFrame(columns=codes)
# # row_val = pd.DataFrame(values[0])
#
# writer = ExcelWriter('test.xlsx')
# # # header_code.to_excel(writer, 'Sheet1')
# # row_period.to_excel(writer, 'Sheet1')
# # # row_val.to_excel(writer, 'Sheet1')
# #
# # # for idx in (0, len(values)):
#
# row_val = pd.DataFrame(values)
# row_val.to_excel(writer, 'Sheet1')
#
# writer.save()
