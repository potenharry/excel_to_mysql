import MySQLdb
import db_config
import time

db_infos = {
    'db1': {'host': db_config.db1['host'], 'port': db_config.db1['port'],
            'user': db_config.db1['user'], 'passwd': db_config.db1['passwd'], 'db': 'intelliquant01'}
}


def update_net_profit(db):
    with open('net_profit.txt', 'r') as f:
        query = ''
        total = 0
        cnt = 0
        while True:
            q = f.readline()
            print('q', q)
            if not q:
                break
            query += q
            cnt += 1
            total += 1
            if cnt == 1000:
                print('cnt 1000')
                try:
                    cursor = db.cursor()
                    cursor.execute(query)
                    cursor.close()
                    query = ''
                except Exception as e:
                    print('mysql error', e)

                cnt = 0
                time.sleep(0.1)


def update_total_equity(db):
    with open('total_equity.txt', 'r') as f:
        query = ''
        total = 0
        cnt = 0
        while True:
            q = f.readline()
            print(q)
            if not q:
                break
            query += q
            cnt += 1
            total += 1
            print('total: ', total)
            if cnt == 1000:
                print('cnt 1000')
                try:
                    cursor = db.cursor()
                    cursor.execute(query)
                    cursor.close()
                    query = ''
                except Exception as e:
                    print('mysql error', e)

                cnt = 0
                time.sleep(0.1)


if __name__ == '__main__':
    db_info = db_infos['db1']
    db = MySQLdb.connect(host=db_info['host'], port=db_info['port'],
                         user=db_info['user'], passwd=db_info['passwd'], db=db_info['db'])
    db.autocommit(True)
    update_net_profit(db)
    update_total_equity(db)
    db.close()
